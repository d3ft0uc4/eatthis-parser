import re

from spinrewriterapi import SpinRewriterAPI

email_address = "as@appercut.eu"
api_key = "2e1aa33#e2edb7d_d22ccf5?623ab9d"

spinrewriter_api = SpinRewriterAPI(email_address, api_key)


def get_unique(lines):
    requests = []
    cnt = 0
    buffer = []
    responses = []
    for line in lines:

        words = line.split(' ')
        cnt += len(words)

        if cnt > 3900:
            requests.append(buffer)
            cnt = 0
            buffer = []

        buffer.append(line)

    if len(buffer):
        requests.append(buffer)

    for request in requests:
        result = spinrewriter_api.get_unique_variation('\n'.join(request))
        if result and result['status'] == 'OK':
            responses.append(result['response'])
        else:
            print(result)
            print("Spin Rewriter API error")
            return
    return '\n'.join(responses)

