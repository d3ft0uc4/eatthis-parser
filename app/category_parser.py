import requests
from bs4 import BeautifulSoup
from db import database


for o in database.articles.find({"category": {"$exists": False}}):
    url = o.get('link')
    print("URL: {}".format(url))
    if not url:
        continue
    elif not url.startswith('http'):
        url = 'https://www.eatthis.com/' + url.lstrip('/')
    try:
        page = requests.get(url)
    except:
        print("Failed parsing url")
        continue
    soup = BeautifulSoup(page.text, 'html.parser')
    category = soup.select_one('a[rel="category tag"]')
    if not category:
        continue
    category = category.get_text()
    o['category'] = category
    database.articles.save(o)


if __name__ == '__main__':
    pass