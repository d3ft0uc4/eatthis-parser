import base64
import requests

user = "admin"
password = "qgzX HEXb 4voN bsoo ZPVR 6s0q"


def get_headers(login, api_key):
    data_string = login + ':' + api_key
    token = base64.b64encode(data_string.encode())

    return {'Authorization': 'Basic ' + token.decode('utf-8')}


def get_domain(url):
    if url:
        return '{}/wp-json/wp/v2/posts'.format(url)
    else:
        return "https://pl-fit.site/wp-json/wp/v2/posts"


def publish_post(title, content, category, date, author, url, login, api_key):
    post = {
        'title': title,
        'status': 'publish',
        'content': content,
        'categories': category or 1,
    }
    if date:
        post['date'] = date
    if author:
        post['author'] = author
    domain = get_domain(url)
    headers = get_headers(login, api_key)
    return requests.post(domain, headers=headers, json=post)
