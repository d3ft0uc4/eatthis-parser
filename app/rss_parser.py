import feedparser
import re
import hashlib
import logging

from bs4 import BeautifulSoup

from db import database

TAGS = []

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

for tag in ['a', 'strong', 'em', 'b', 'span']:
    TAGS.append(re.compile(r'<{}.*?>'.format(tag)))
    TAGS.append(re.compile(r'</{}>'.format(tag)))


def remove_links(text):
    for r in TAGS:
        text = r.sub('', text)
    for ch in ['\t', '\xa0', '<img />']:
        text = text.replace(ch, '')
    return text


page = 1

while True:
    URL = 'https://www.eatthis.com/feed/?paged={}'.format(page)

    logger.info('Parsing page: {}'.format(URL))

    data = feedparser.parse(URL)
    if not data.entries:
        break
    for entry in data.entries:
        html = remove_links(entry['content'][0]['value']).split('\n')
        html = list(filter(lambda part: part.strip(), html))
        html = html[0:-1]

        cleantext = BeautifulSoup('\n'.join(html), "lxml").text.split('\n')

        author = entry['author']

        tags = list(map(lambda tag: tag['term'], entry['tags']))
        title = entry['title']
        link = entry['link']
        published = entry['published_parsed']

        obj = dict(author=author,
                   html=html,
                   clean_text=cleantext,
                   tags=tags,
                   title=title,
                   link=link,
                   _id=hashlib.md5(title.encode('utf-8')).hexdigest(),
                   published=published)

        database.articles.save(obj)

    page += 1

if __name__ == '__main__':
    pass
