from pymongo import MongoClient

client = MongoClient('mongodb://127.0.0.1:27017/')
database = client.db

projection = {'clean_text': 0, 'author': 0, 'published': 0, 'link': 0}