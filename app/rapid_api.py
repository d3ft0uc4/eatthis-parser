import requests
import json

url = "https://contextualwebsearch-websearch-v1.p.rapidapi.com/api/Search/ImageSearchAPI"


def get_similar_image(q):
    try:
        querystring = {"q": q, "pageNumber": "1", "pageSize": "10", "autoCorrect": "true"}

        headers = {
            'x-rapidapi-key': "7f07df0badmsh0202197f36d6826p108029jsn96d22507cbef",
            'x-rapidapi-host': "contextualwebsearch-websearch-v1.p.rapidapi.com"
        }

        response = requests.request("GET", url, headers=headers, params=querystring)
        image = json.loads(response.text).get('value', [])
        return image[0]['url'] if image else None
    except Exception as e:
        print(str(e))


if __name__ == '__main__':
    print(get_similar_image("taylor swift"))
