from spinrewriter import get_unique
from rapid_api import get_similar_image
from bs4 import BeautifulSoup


def make_unique(a):
    soup = BeautifulSoup('\n'.join(a['html']), "html.parser")
    lines = []
    for t in soup.findAll(text=True):
        text = t.strip()
        if text:
            lines.append(text)

    unique_text = get_unique(lines)

    if unique_text:
        unique_parts = unique_text.split('\n')
        for t in soup.findAll(text=True):
            if not len(unique_parts):
                break
            text = t.strip()
            if text:
                t.replaceWith(unique_parts.pop(0))
    soup = remove_attrs(soup)
    soup = process_images(soup)
    a['html'] = str(soup)

    return a

def remove_attrs(soup):
    whitelist = ['img']
    for tag in soup.find_all(True):
        if tag.name not in whitelist:
            tag.attrs = {}
        else:
            attrs = dict(tag.attrs)
            for attr in attrs:
                if attr not in ['src', 'alt']:
                    del tag.attrs[attr]
    return soup


def process_images(soup):
    whitelist = ['img']
    for tag in soup.find_all(True):
        if tag.name == 'img':
            attrs = dict(tag.attrs)
            if tag.attrs.get('alt'):
                similar_image = get_similar_image(tag.attrs.get('alt'))
                if similar_image:
                    tag.attrs['src'] = similar_image
    return soup