from apscheduler.executors.pool import ThreadPoolExecutor, ProcessPoolExecutor
from bs4 import BeautifulSoup
from flask import Flask, request, jsonify, Response
from apscheduler.schedulers.background import BackgroundScheduler
from db import database, projection
from wordpress_api import publish_post
import secrets

from helpers import remove_attrs, make_unique
from flask_cors import CORS, cross_origin

app = Flask(__name__)

CORS(app, resources={r'/*': {'origins': '*'}})
app.config['CORS_HEADERS'] = 'Content-Type'

executors = {
    'default': ThreadPoolExecutor(20),
    'processpool': ProcessPoolExecutor(5)
}
job_defaults = {
    'coalesce': False,
    'max_instances': 1
}


def tick():
    run = database.runs.find_one({'status': 'created'})
    if not run:
        return
    run['status'] = 'in_progress'
    database.runs.save(run)
    data = run['data']
    api_key = data.get('api_key', '')
    login = data.get('login', '')
    url = data.get('url', '')
    category = data.get('category', '')
    posts = data.get('posts', [])

    for post in posts:
        article_id = post.get('id')
        author = post.get('author')
        publish_date = post.get('publish_date')
        article = database.articles.find_one({"_id": article_id})
        if not article:
            print("Article with id {} not found".format(article_id))
            continue
        try:
            article = make_unique(article)
            publish_post(title=article['title'],
                         content=article['html'],
                         category=category,
                         date=publish_date,
                         author=author,
                         url=url,
                         login=login,
                         api_key=api_key)
        except Exception as e:
            print(str(e))
    run['status'] = 'finished'
    database.runs.save(run)


scheduler = BackgroundScheduler(daemon=True, executors=executors, job_defaults=job_defaults)
scheduler.add_job(tick, 'interval', seconds=10)
scheduler.start()

app = Flask(__name__)


@app.route("/articles")
@cross_origin()
def articles():
    skip = int(request.values.get('skip', 0))
    limit = int(request.values.get('limit', 20))
    query = {}
    projection = {'category': 1, 'tags': 1, 'title': 1}
    title = request.values.get('title')
    tags = request.values.get('tags')
    category = request.values.get('category')

    if title:
        query['title'] = {'$regex': title, '$options': 'i'}

    if tags:
        query['tags'] = {'$all': tags.split(',')}

    if category:
        query['category'] = {'$regex': category, '$options': 'i'}

    return jsonify({
        'articles': list(database.articles.find(query, projection).skip(skip).limit(limit)),
        'total': database.articles.find(query).count()})


@app.route("/articles/<_id>")
def article(_id):
    a = list(database.articles.find({'_id': _id}, projection))
    if len(a):
        a = a[0]
        soup = BeautifulSoup('\n'.join(a['html']), "html.parser")
        soup = remove_attrs(soup)
        a['html'] = str(soup)
        return jsonify(a)
    return Response(status=404)


@app.route("/categories")
@cross_origin()
def categories():
    return jsonify(list(database.articles.distinct('category')))


@app.route("/tags")
@cross_origin()
def tags():
    return jsonify(list(database.articles.distinct('tags')))


@app.route("/articles/<_id>/unique")
@cross_origin()
def get_unique_article(_id):
    a = list(database.articles.find({'_id': _id}, projection))
    if not a:
        return Response(status=404)
    return jsonify(make_unique(a[0]))


@app.route("/run", methods=['POST'])
@cross_origin()
def create_run():
    data = request.get_json(force=True)
    data = data
    _id = secrets.token_hex(nbytes=16)
    _id = database.runs.insert({'_id': _id, 'data': data, 'status': 'created'})
    return jsonify({'_id': _id})


@app.route("/run", methods=['GET'])
@cross_origin()
def get_runs():
    return jsonify(list(database.runs.find({})))


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)
